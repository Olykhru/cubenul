/*
	AJOUTER BACKGROUND TEMPS POUR CHAQUE TITRE -> "#divtemps.width = xx.xx%"
*/
$(document).ready(function()
{

	var play = document.getElementsByClassName('play'),
		shuffledPlay,
		volumeImgs = document.getElementsByClassName('volumeImg'),
		id,
		src,
		vol,
		img,
		nbrMp3 = 0,
		position,
	  musiques = ['tokyo',
			'seoul',
			'moonchild',
			'badbye',
			'uhgood',
			'everythingoes',
			'forever'],
		musiques2 = ['',
			' (prod. HONNE)',
			'',
			' (with eAeon)',
			'',
			' (with NELL)',
			' rain',],
		audio = $('audio')[0],
		celuila = $(play)[0],   //"this" c'est pas assez la France oui
		currentM,  //variable qui indiquera la minute actuelle dans la musique pour l'input range
		currentS,  //variable qui indiquera la seconde actuelle de la minute actuelle dans la musique pour l'input range
		dureeM,
		dureeS,
		alea = false,
		boucle = 0, //0 -> pas de boucle | 1 -> album en boucle | 2 -> chanson en boucle
		maj = new Event('maj'),
		aleaImg = document.getElementById('alea').getElementsByTagName('img')[0],
		boucleImg = document.getElementById('boucle').getElementsByTagName('img')[0],
		i = 0,
		j = 0;

	/* PrÃ©chargement des images */

	img0 = new Image();
	img1 = new Image();
	img0.src = 'IMG/Pause.png';
	img1.src = 'IMG/boucle1.png';

	/* Lecteur et tableau de musiques */

	id = $(play).index(celuila);
	src = musiques[id];


	for (j = 0; j < 7; j++)
	{
		celuila = $(play)[j];
		if(j == id)
		{
			$(celuila).addClass('lecture');
		}
		else
		{
			celuila.getElementsByTagName('img')[0].src = 'IMG/Play.png';
			$(celuila).removeClass('lecture');
		}
	}

	celuila = $(play)[id];

	for (i = 0; i < play.length; i++) //ajout des event listener pour les clics sur chaque ligne du tableau
	{
		$(play[i]).click(function(event)
		{
			if(event.target.className != 'dl')
			{
				onclickPlay(this);
			}
		});
	}

	if(!(new RegExp(src).test($('source')[0].src)))
	{
		audio.pause();
		audio.currentTime = 0;

		$('source')[0].src = 'M/0' + (id + 1) + '_' + src + musiques2[id] + '.mp3';

		audio.load();
	}

	volumeChange();

	$('#alea').on('click', function()
	{
		alea = !alea;
		if(alea)
		{
			if(boucle == 0)
			{
				$(boucleImg).css('opacity', '0.8');
				boucle = 1;
			}
			$(aleaImg).css('opacity', '0.8');
			shuffledPlay = shuffle([1,2,3,4,5,6,7]);
			updatePosition();
			if(audio.paused)
			{
				position = 0;
				onclickPlay(play[$(shuffledPlay).index(position + 1)]);
			}
			else
			{
				position = parseInt(celuila.getElementsByClassName('position')[0].innerText) - 1;
			}
		}
		else
		{
			$(aleaImg).css('opacity', '0.3');
			$(play).each(function()
			{
				this.getElementsByClassName('position')[0].innerText = ($(play).index(this)) + 1;
			});
		}
	});

	$('#boucle').on('click', function()
	{
		if(boucle == 0)
			$(boucleImg).css('opacity', '0.8');
		else if (boucle == 1)
		{
			$(boucleImg).attr('src', 'IMG/boucle1.png')
		}
		else
		{
			$(boucleImg).css('opacity', '0.3');
			$(boucleImg).attr('src', 'IMG/boucle.png')
		}
		boucle = (boucle + 1) % 3;
	});

	$('#mp3').on('maj', function()
	{
		zipMp3.generateAsync({type:'blob'}).then(function(blob)
		{
			saveAs(blob, 'RM-Mono.zip')
		}, function(err)
		{
			var a = $('#mp3').text();
			$('#mp3').text(err);
			setTimeout(function()
			{
				$('#mp3').text(a);
			}, 10000);
		});
		nbrMp3 = 0;
		$('#compressionMP3Val').trigger('maj')
		$('#compression').css('display', 'none');
		$('#compressionMP3').css('display', 'none');
	});

	$('#compressionMP3Val').on('maj', function()
	{
		$(this).text(nbrMp3 + '/7');
		$(this).css('width', (nbrMp3 / 7) * 100 + '%');
		$('#compression').css('display', 'flex');
		$('#compressionMP3').css('display', 'block');
		if(nbrMp3 == 7)
		{
			$('#mp3').trigger('maj');
		}
	});

	$('#precedante').on('click', function()
	{
		if (alea)
		{
			position = (position == 0 ? 10 : (position - 1));
			onclickPlay($(play)[$(shuffledPlay).index(position + 1)]);
		}
		else
		{
			onclickPlay($(play)[(id == 0 ? 10 : id - 1)]);
		}
	});

	$('#suivante').on('click', function()
	{
		if (alea)
		{
			position = (position + 1) % 7;
			onclickPlay($(play)[$(shuffledPlay).index(position + 1)]);
		}
		else
		{
			onclickPlay($(play)[id + 1]);
		}
	});

	$('#playPauseBtn').click(function()
	{
		$(audio).on('canplay', function()
		{
			audio.play();
			celuila.getElementsByTagName('img')[0].src = 'IMG/Pause.png';
			$('#playPauseImg').attr('src', 'IMG/Pause.png');
		});
		if(!audio.paused)
		{
			audio.pause();
			celuila.getElementsByTagName('img')[0].src = 'IMG/Play.png';
			$('#playPauseImg').attr('src', 'IMG/Play.png');
		}
		else
		{
			audio.play();
			celuila.getElementsByTagName('img')[0].src = 'IMG/Pause.png';
			$('#playPauseImg').attr('src', 'IMG/Pause.png');
		}
	});

	$('#seekSlider').on('input', function()
	{
		audio.currentTime = audio.duration * ($('#seekSlider').val() / 100);
		currentM = Math.floor(audio.currentTime / 60);
		currentS = Math.floor(audio.currentTime - (currentM * 60));
		dureeM = Math.floor(audio.duration / 60);
		dureeS = Math.floor(audio.duration - (dureeM * 60));
		if(currentM < 10)
		{
			currentM = '0' + currentM;
		}
		if(currentS < 10)
		{
			currentS = '0' + currentS;
		}
		if(dureeM < 10)
		{
			dureeM = '0' + dureeM;
		}
		if(dureeS < 10)
		{
			dureeS = '0' + dureeS;
		}
		$('#curTimeText').text(currentM + ':' + currentS);
		$('#durTimeText').text(dureeM + ':' + dureeS);
	});

	$('#volumeImgs').click(function()
	{
		if(audio.volume == 0)
		{
			$('#volumeSlider').val(vol);
			volumeChange();
		}
		else
		{
			vol = $('#volumeSlider').val();
			$('#volumeSlider').val(0);
			volumeChange();
		}
	});

	$('#volumeSlider').on('input', function()
	{
		volumeChange();
	});

	$(audio).on('loadstart', function()
	{
		celuila.getElementsByTagName('img')[0].src = 'IMG/Loading.gif';
		$('#playPauseImg').attr('src', 'IMG/Loading.gif');
	});
	$(audio).on('loadedmetadata', function()
	{
		dureeM = Math.floor(audio.duration / 60);
		dureeS = Math.floor(audio.duration - (dureeM * 60));
		if(dureeM < 10)
		{
			dureeM = '0' + dureeM;
		}
		if(dureeS < 10)
		{
			dureeS = '0' + dureeS;
		}
		$('#durTimeText').text(dureeM + ':' + dureeS);
	});
	$(audio).on('canplay', function()
	{
		celuila.getElementsByTagName('img')[0].src = 'IMG/Play.png';
		$('#playPauseImg').attr('src', 'IMG/Play.png');
	});
	$(audio).on('timeupdate', function()
	{
		let percent = 100 * audio.currentTime / audio.duration;
		$('#seekSlider').val((Number.isNaN(audio.duration) ? 0 : percent));
		$($('tr')[id + 1]).css('background-image', 'linear-gradient(to right, #e6e4e1aa ' + percent + '%, transparent ' + (percent + 1) + '%)');
		currentM = Math.floor(audio.currentTime / 60);
		currentS = Math.floor(audio.currentTime - (currentM * 60));
		dureeM = Math.floor(audio.duration / 60);
		dureeS = Math.floor(audio.duration - (dureeM * 60));
		if(currentM < 10)
		{
			currentM = '0' + currentM;
		}
		if(currentS < 10)
		{
			currentS = '0' + currentS;
		}
		if(dureeM < 10)
		{
			dureeM = '0' + dureeM;
		}
		if(dureeS < 10)
		{
			dureeS = '0' + dureeS;
		}
		$('#curTimeText').text(currentM + ':' + currentS);
		$('#durTimeText').text(dureeM + ':' + dureeS);
		if(audio.ended)
		{
			if(boucle == 2)
			{
				audio.currentTime = 0;
				audio.play();
			}
			else if (boucle == 1)
			{
				if (alea)
				{
					position = (position + 1) % 7;
					onclickPlay($(play)[$(shuffledPlay).index(position + 1)]);
				}
				else
				{
					onclickPlay($(play)[id % 7]);
				}
			}
			else
			{
				celuila.getElementsByTagName('img')[0].src = 'IMG/Play.png';
				$('#playPauseImg').attr('src', 'IMG/Play.png');
			}
		}
	});

	function onclickPlay(oui)
	{
		id = $(play).index(oui);
		position = parseInt(oui.getElementsByClassName('position')[0].innerText) - 1;
		celuila = $(play)[id];
		src = musiques[id];

		$(document).attr('title', celuila.getElementsByTagName('td')[2].innerText);

		$(audio).on('canplay', function()
		{
			audio.play();
			celuila.getElementsByTagName('img')[0].src = 'IMG/Pause.png';
			$('#playPauseImg').attr('src', 'IMG/Pause.png');
		});

		for (j = 0; j < 7; j++)
		{
			celuila = $(play)[j];
			if(j == id)
			{
				celuila.getElementsByTagName('img')[0].src = 'IMG/Pause.png';
				$(celuila).css('background-image', 'linear-gradient(to right, #e6e4e1aa 0%, transparent 1%)');
				$(celuila).addClass('lecture');
			}
			else
			{
				celuila.getElementsByTagName('img')[0].src = 'IMG/Play.png';
				$(celuila).css('background-image', 'linear-gradient(to right, transparent, transparent)');
				$(celuila).removeClass('lecture');
			}
		}

		celuila = $(play)[id];

		if(!(new RegExp(src).test($('source')[0].src)))
		{
			audio.pause();
			audio.currentTime = 0;

			$('source')[0].src = 'M/0' + (id + 1) + '_' + src + musiques2[id] + '.mp3';

			audio.load()
		}
		else
		{
			if(!audio.paused)
			{
				audio.pause();
				celuila.getElementsByTagName('img')[0].src = 'IMG/Play.png';
				$('#playPauseImg').attr('src', 'IMG/Play.png');
			}
			else
			{
				audio.play();
				celuila.getElementsByTagName('img')[0].src = 'IMG/Pause.png';
				$('#playPauseImg').attr('src', 'IMG/Pause.png');
			}
		}
	}

	function updatePosition()
	{
		for(i = 0; i < shuffledPlay.length; i++)
		{
			play[i].getElementsByClassName('position')[0].innerText = shuffledPlay[i];
		}
	}

	function volumeChange()
	{
		audio.volume = $('#volumeSlider').val() / 30000;

		if($('#volumeSlider').val() > 20000)
		{
			$(volumeImgs[1]).fadeTo(0, ($('#volumeSlider').val() - 20000) / 10000);
			$(volumeImgs[2]).fadeTo(0, 1);
			$(volumeImgs[3]).fadeTo(0, 1);
		}
		else
		{
			$(volumeImgs[1]).fadeTo(0, 0);
		}

		if($('#volumeSlider').val() > 10000)
		{
			if($('#volumeSlider').val() <= 20000)
			{
				$(volumeImgs[2]).fadeTo(0, ($('#volumeSlider').val() - 10000) / 10000);
				$(volumeImgs[3]).fadeTo(0, 1);
			}
		}
		else
		{
			$(volumeImgs[2]).fadeTo(0, 0);
		}

		if($('#volumeSlider').val() > 0)
		{
			if($('#volumeSlider').val() <= 10000)
			{
				$(volumeImgs[3]).fadeTo(0, $('#volumeSlider').val() / 10000);
			}
		}
		else
		{
			$(volumeImgs[3]).fadeTo(0, 0);
		}

		if($('#volumeSlider').val() == 0)
		{
			$(volumeImgs[4]).fadeTo(0, 1);
		}
		else
		{
			$(volumeImgs[4]).fadeTo(0, 0);
		}
	}

	function shuffle(array)
	{
	  var m = array.length,
			t,
			i;

	  // While there remain elements to shuffleâ¦
	  while (m)
		{
	    // Pick a remaining elementâ¦
	    i = Math.floor(Math.random() * m--);

	    // And swap it with the current element.
	    t = array[m];
	    array[m] = array[i];
	    array[i] = t;
	  }

	  return array;
	}

	/* Zip */

	var zipMp3 = new JSZip();

	$('#mp3').on('click', function()
	{
		if(nbrMp3 == 0)
		{
			$('#compressionMP3Val').trigger('maj');
			JSZipUtils.getBinaryContent(('M/01_' + musiques[0] + musiques2[0] + '.mp3'), function (err, data)
			{
		    if(err)
				{
		    	throw err;
		    }
		    zipMp3.file('RM-Mono/01_' + musiques[0] + musiques2[0] + '.mp3', data, {binary:true});
				nbrMp3++;
				$('#compressionMP3Val').trigger('maj');
		  });
			JSZipUtils.getBinaryContent(('M/02_' + musiques[1] + musiques2[1] + '.mp3'), function (err, data)
			{
		    if(err)
				{
		    	throw err;
		    }
		    zipMp3.file('RM-Mono/02_' + musiques[1] + musiques2[1] + '.mp3', data, {binary:true});
				nbrMp3++;
				$('#compressionMP3Val').trigger('maj');
		  });
			JSZipUtils.getBinaryContent(('M/03_' + musiques[2] + musiques2[2] + '.mp3'), function (err, data)
			{
		    if(err)
				{
		    	throw err;
		    }
		    zipMp3.file('RM-Mono/03_' + musiques[2] + musiques2[2] + '.mp3', data, {binary:true});
				nbrMp3++;
				$('#compressionMP3Val').trigger('maj');
		  });
			JSZipUtils.getBinaryContent(('M/04_' + musiques[3] + musiques2[3] + '.mp3'), function (err, data)
			{
		    if(err)
				{
		    	throw err;
		    }
		    zipMp3.file('RM-Mono/04_' + musiques[3] + musiques2[3] + '.mp3', data, {binary:true});
				nbrMp3++;
				$('#compressionMP3Val').trigger('maj');
		  });
			JSZipUtils.getBinaryContent(('M/05_' + musiques[4] + musiques2[4] + '.mp3'), function (err, data)
			{
		    if(err)
				{
		    	throw err;
		    }
		    zipMp3.file('RM-Mono/05_' + musiques[4] + musiques2[4] + '.mp3', data, {binary:true});
				nbrMp3++;
				$('#compressionMP3Val').trigger('maj');
		  });
			JSZipUtils.getBinaryContent(('M/06_' + musiques[5] + musiques2[5] + '.mp3'), function (err, data)
			{
		    if(err)
				{
		    	throw err;
		    }
		    zipMp3.file('RM-Mono/06_' + musiques[5] + musiques2[5] + '.mp3', data, {binary:true});
				nbrMp3++;
				$('#compressionMP3Val').trigger('maj');
		  });
			JSZipUtils.getBinaryContent(('M/07_' + musiques[6] + musiques2[6] + '.mp3'), function (err, data)
			{
		    if(err)
				{
		    	throw err;
		    }
		    zipMp3.file('RM-Mono/07_' + musiques[6] + musiques2[6] + '.mp3', data, {binary:true});
				nbrMp3++;
				$('#compressionMP3Val').trigger('maj');
		  });
		}
	});

});
